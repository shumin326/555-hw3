package edu.upenn.cis.stormlite.mapreduce;

import java.util.HashMap;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.fasterxml.jackson.databind.ObjectMapper;

import edu.upenn.cis.stormlite.Config;
import edu.upenn.cis.stormlite.TopologyContext.STATE;
import edu.upenn.cis.stormlite.distributed.WorkerHelper;
import edu.upenn.cis.stormlite.distributed.WorkerJob;
import edu.upenn.cis455.mapreduce.master.MasterServer;
import edu.upenn.cis455.mapreduce.master.routes.SubmitJobRoute;
import edu.upenn.cis455.mapreduce.worker.WorkerStatus;
import junit.framework.TestCase;

/**
 * Simple word counter test case, largely derived from
 * https://github.com/apache/storm/tree/master/examples/storm-mongodb-examples
 * 
 */
/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
public class TestMapReduce extends TestCase {
	static Logger log = LogManager.getLogger(TestMapReduce.class);
	private Config config = new Config();
	private boolean test = false; // change this to true when doing manually test.

	@Before
	public void setUp() {
		if (!test)
			return;
		// launch the master server
		new MasterServer(45555);
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		// launch two workers
		while (MasterServer.getWorkerStatus().size() != 2) {
			log.warn(
					"Waiting for two workers to start...");
			try {
				Thread.sleep(3000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		String workerList = MasterServer.getWorkerList();
		config.put("workerList", workerList);
	}

	@Test
	public void testStatusUpdate() {
		if (!test)
			return;
		// create job.
		log.info("************ Creating the job request ***************");
		createSampleMapReduce(config);
		WorkerJob job = new WorkerJob();
		assertTrue(SubmitJobRoute.createJob(config, job));
		ObjectMapper mapper = new ObjectMapper();
		mapper.enableDefaultTyping(ObjectMapper.DefaultTyping.NON_FINAL);

		// send job info
		log.info("************ Sending the job request ***************");
		assertTrue(SubmitJobRoute.sendJob(job, WorkerHelper.getWorkers(config)));
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		// start job
		log.info("************ Starting job ***************");
		assertTrue(SubmitJobRoute.startJob(WorkerHelper.getWorkers(config)));
		try {
			Thread.sleep(5000); // wait for next round of update
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		// Check status update after running the job.
		Map<String, WorkerStatus> workerStatusMap = MasterServer.getWorkerStatus();
		assertEquals(2, workerStatusMap.size());
		for (Map.Entry<String, WorkerStatus> entry : workerStatusMap.entrySet()) {
			WorkerStatus workerStatus = entry.getValue();
			assertEquals(String.format("Worker%d job is wrong!",
					workerStatus.getIndex()), "MyJob1",
					workerStatus.getJob());
			assertEquals(String.format("Worker%d status is wrong!",
					workerStatus.getIndex()), STATE.IDLE,
					workerStatus.getState());
			assertEquals(String.format("Worker%d keysRead is wrong!",
					workerStatus.getIndex()), 0,
					workerStatus.getKeysRead());
			if (workerStatus.getIndex() == 0) {
				Map<String, Integer> expectedRes = new HashMap<>();
				getRes(expectedRes);
				String actualRes = workerStatus.getResults();
				for (Map.Entry<String, Integer> res : expectedRes.entrySet()) {
					String actualKeyValue = actualRes.substring(actualRes.indexOf(res.getKey()));
					actualKeyValue = actualKeyValue.substring(0, actualKeyValue.indexOf(")"));
					Integer actualValue = Integer.valueOf(actualKeyValue.substring(actualKeyValue.indexOf(",") + 1));
					assertEquals(String.format("Worker%d result of %s is wrong!",
							workerStatus.getIndex(), res.getKey()), res.getValue(), actualValue);
				}
			} else {
				assertEquals(String.format("Worker%d results is wrong!",
						workerStatus.getIndex()), "[]", workerStatus.getResults());
			}
		}
	}

	@After
	public void tearDown() {
		if (!test)
			return;
		log.info("************ Shutting down the workers ***************");
		assertTrue(MasterServer.shutdown());
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	static void createSampleMapReduce(Config config) {
		// Job name
		config.put("job", "MyJob1");

		// Class with map function
		config.put("mapClass", "edu.upenn.cis455.mapreduce.job.WordCount");
		// Class with reduce function
		config.put("reduceClass", "edu.upenn.cis455.mapreduce.job.WordCount");
		config.put("input", "");
		config.put("output", "");

		// Numbers of executors (per node)
		config.put("spoutExecutors", "1");
		config.put("mapExecutors", "1");
		config.put("reduceExecutors", "1");

	}

	private void getRes(Map<String, Integer> res) {
		res.clear();
		res.put("barked", 1);
		res.put("need", 1);
		res.put("over", 1);
		res.put("the", 5);
		res.put("how", 1);
		res.put("tries", 1);
		res.put("quick", 1);
		res.put("does", 1);
		res.put("lazy", 1);
		res.put("jumped", 1);
		res.put("dog", 3);
		res.put("and", 1);
		res.put("over", 1);
		res.put("the", 5);
		res.put("how", 1);
		res.put("tries", 1);
		res.put("quick", 1);
		res.put("does", 1);
		res.put("lazy", 1);
		res.put("jumped", 1);
		res.put("dog", 3);
		res.put("back", 1);
		res.put("catch", 1);
		res.put("to", 1);
		res.put("brown", 1);
		res.put("many", 1);
		res.put("fox", 2);
		res.put("barked", 1);
		res.put("need", 1);
		res.put("and", 1);
		res.put("back", 1);
		res.put("catch", 1);
		res.put("to", 1);
		res.put("brown", 1);
		res.put("many", 1);
		res.put("fox", 2);
	}

}
