package edu.upenn.cis455.mapreduce.master.routes;

import edu.upenn.cis.stormlite.TopologyContext.STATE;
import edu.upenn.cis455.mapreduce.master.MasterServer;
import edu.upenn.cis455.mapreduce.worker.WorkerStatus;
import spark.Request;
import spark.Response;
import spark.Route;

import java.net.HttpURLConnection;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class WorkerStatusRoute implements Route {
    static Logger log = LogManager.getLogger(WorkerStatusRoute.class);

    private Map<String, WorkerStatus> status;

    public WorkerStatusRoute(Map<String, WorkerStatus> status) {
        this.status = status;
    }

    @Override
    public Object handle(Request request, Response response) throws Exception {
        log.info("Received worker status update request");
        if (request.queryParams("port") == null || request.queryParams("port").equals("")) {
            response.status(HttpURLConnection.HTTP_BAD_REQUEST);
            return "Bad request: missing port";
        }
        if (request.queryParams("status") == null || request.queryParams("status").equals("")) {
            response.status(HttpURLConnection.HTTP_BAD_REQUEST);
            return "Bad request: missing status";
        }
        if (request.queryParams("job") == null || request.queryParams("job").equals("")) {
            response.status(HttpURLConnection.HTTP_BAD_REQUEST);
            return "Bad request: missing job";
        }
        if (request.queryParams("keysRead") == null || request.queryParams("keysRead").equals("")) {
            response.status(HttpURLConnection.HTTP_BAD_REQUEST);
            return "Bad request: missing keysRead";
        }
        if (request.queryParams("keysWritten") == null || request.queryParams("keysWritten").equals("")) {
            response.status(HttpURLConnection.HTTP_BAD_REQUEST);
            return "Bad request: missing keysWritten";
        }
        if (request.queryParams("results") == null || request.queryParams("results").equals("")) {
            response.status(HttpURLConnection.HTTP_BAD_REQUEST);
            return "Bad request: missing results";
        }

        STATE state;
        switch (request.queryParams("status").toUpperCase()) {
            case "INIT":
                state = STATE.INIT;
                break;
            case "MAPPING":
                state = STATE.MAPPING;
                break;
            case "REDUCING":
                state = STATE.REDUCING;
                break;
            case "IDLE":
                state = STATE.IDLE;
                break;
            default:
                state = STATE.INIT;
                break;
        }
        String workerAddr = request.ip() + ":" + request.queryParams("port");
        int workerIndex;
        if (!status.containsKey(workerAddr)) {
            workerIndex = status.size();
            status.put(workerAddr, new WorkerStatus());
        } else {
            workerIndex = status.get(workerAddr).getIndex();
        }
        status.get(workerAddr).update(
                state,
                request.queryParams("job"),
                Integer.valueOf(request.queryParams("keysRead")),
                Integer.valueOf(request.queryParams("keysWritten")),
                request.queryParams("results"),
                Integer.valueOf(request.queryParams("port")),
                workerIndex);
        return "worker" + workerIndex + "status updated!";
    }
}
