package edu.upenn.cis455.mapreduce.master;

import static spark.Spark.*;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import edu.upenn.cis.stormlite.Config;
import edu.upenn.cis.stormlite.distributed.WorkerHelper;
import edu.upenn.cis455.mapreduce.master.routes.ShutdownRoute;
import edu.upenn.cis455.mapreduce.master.routes.StatusRoute;
import edu.upenn.cis455.mapreduce.master.routes.SubmitJobRoute;
import edu.upenn.cis455.mapreduce.master.routes.WorkerStatusRoute;
import edu.upenn.cis455.mapreduce.worker.WorkerStatus;

public class MasterServer {
    public static final String FILE_SPOUT = "fileSpout";
    public static final String MAP_BOLT = "mapBolt";
    public static final String REDUCE_BOLT = "reduceBolt";
    public static final String PRINT_BOLT = "printBolt";

    private static Config config = new Config();
    private static Map<String, WorkerStatus> workerStatus = new HashMap<>();

    public static String getWorkerList() {
        String[] workerList = new String[workerStatus.size()];
        for (Map.Entry<String, WorkerStatus> entry : workerStatus.entrySet()) {
            WorkerStatus workerStatus = entry.getValue();
            String workerAddr = entry.getKey();
            if (!workerAddr.startsWith("http://"))
                workerAddr = "http://" + workerAddr;
            workerList[workerStatus.getIndex()] = workerAddr;
        }
        return Arrays.toString(workerList);
    }

    public static Map<String, WorkerStatus> getWorkerStatus() {
        return workerStatus;
    }

    public static boolean shutdown() {
        for (String worker : WorkerHelper.getWorkers(config)) {
            URL url;
            try {
                url = new URL(worker + "/shutdown");
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setDoOutput(true);
                conn.setRequestMethod("GET");
                if (conn.getResponseCode() != HttpURLConnection.HTTP_OK)
                    return false;
            } catch (IOException e) {
                e.printStackTrace();
                return false;
            }
        }
        return true;
    }

    public MasterServer(int port) {
        port(port);

        config.put("workerList", "[]");

        System.out.println("Master node startup, on port " + port);

        // Route handler for /status shows all active worker information as well as a
        // form to submit new job.
        get("/status", new StatusRoute(workerStatus));

        // Route handler for /submitjob creates job configuration.
        post("/submitjob", new SubmitJobRoute(config));

        // Route handler for /workerstatus reports from the workers.
        get("/workerstatus", new WorkerStatusRoute(workerStatus));

        // Route handler for /shutdown shutsdown the whole cluster.
        get("/shutdown", new ShutdownRoute(WorkerHelper.getWorkers(config)));
    }

    /**
     * The mainline for launching a MapReduce Master. This should
     * handle at least the status and workerstatus routes, and optionally
     * initialize a worker as well.
     * 
     * @param args
     */
    public static void main(String[] args) {
        if (args.length < 1) {
            System.out.println("Usage: MasterServer [port number]");
            System.exit(1);
        }

        int myPort = Integer.valueOf(args[0]);
        new MasterServer(myPort);
    }
}
