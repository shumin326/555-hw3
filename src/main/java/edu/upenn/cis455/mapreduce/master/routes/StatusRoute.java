package edu.upenn.cis455.mapreduce.master.routes;

import java.util.Map;

import edu.upenn.cis455.mapreduce.worker.WorkerStatus;
import spark.Request;
import spark.Response;
import spark.Route;

public class StatusRoute implements Route {

    private Map<String, WorkerStatus> status;

    public StatusRoute(Map<String, WorkerStatus> status) {
        this.status = status;
    }

    @Override
    public Object handle(Request request, Response response) throws Exception {
        response.type("text/html");
        String body = "";
        body += "<html><head><title>Master</title></head><body>";
        body += "<div>Full name: Shumin Yuan</div>";
        body += "<div>SEAS login: shumin</div>";

        // Add worker status information
        for (Map.Entry<String, WorkerStatus> entry : status.entrySet()) {
            WorkerStatus workerStatus = entry.getValue();
            if (workerStatus.isActive()) {
                body += "<div>" + workerStatus.getIndex() + ": port=" + workerStatus.getPort();
                body += ", status=" + workerStatus.getState();
                body += ", job=" + workerStatus.getJob();
                body += ", keysRead=" + workerStatus.getKeysRead();
                body += ", keysWritten=" + workerStatus.getKeysWritten();
                body += ", results=" + workerStatus.getResults() + "</div>";
            }
        }

        // Add job creation form.
        body += "<form method=\"POST\" action=\"/submitjob\">";
        body += "Job Name: <input type=\"text\" name=\"jobname\"/><br/>";
        body += "Class Name: <input type=\"text\" name=\"classname\"/><br/>";
        body += "Input Directory: <input type=\"text\" name=\"input\"/><br/>";
        body += "Output Directory: <input type=\"text\" name=\"output\"/><br/>";
        body += "Map Threads: <input type=\"text\" name=\"map\"/><br/>";
        body += "Reduce Threads: <input type=\"text\" name=\"reduce\"/><br/>";
        body += "<input type=\"submit\" value=\"Submit\"></form>";

        body += "</body></html>";
        return body;
    }

}
