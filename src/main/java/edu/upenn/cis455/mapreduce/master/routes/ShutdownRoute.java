package edu.upenn.cis455.mapreduce.master.routes;

import spark.Request;
import spark.Response;
import spark.Route;

import java.net.HttpURLConnection;
import java.net.URL;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class ShutdownRoute implements Route {
    static Logger log = LogManager.getLogger(ShutdownRoute.class);

    String[] workers;

    public ShutdownRoute(String[] workers) {
        this.workers = workers;
    }

    @Override
    public Object handle(Request request, Response response) throws Exception {
        int workerIndex = 0;
        for (String worker : workers) {
            URL url = new URL(worker + "/shutdown");
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setDoOutput(true);
            conn.setRequestMethod("GET");

            int statusCode = conn.getResponseCode();
            if (statusCode != HttpURLConnection.HTTP_OK) {
                response.status(statusCode);
                return "Worker " + workerIndex + "failed to shutdown, status code: " + statusCode;
            }
            workerIndex += 1;
        }
        return "Shutdown success!";
    }

}
