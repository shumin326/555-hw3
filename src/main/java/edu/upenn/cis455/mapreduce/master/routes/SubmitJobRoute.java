package edu.upenn.cis455.mapreduce.master.routes;

import spark.Request;
import spark.Response;
import spark.Route;

import java.io.IOException;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import com.fasterxml.jackson.databind.ObjectMapper;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import edu.upenn.cis.stormlite.Config;
import edu.upenn.cis.stormlite.Topology;
import edu.upenn.cis.stormlite.TopologyBuilder;
import edu.upenn.cis.stormlite.bolt.MapBolt;
import edu.upenn.cis.stormlite.bolt.MyPrintBolt;
import edu.upenn.cis.stormlite.bolt.ReduceBolt;
import edu.upenn.cis.stormlite.distributed.WorkerHelper;
import edu.upenn.cis.stormlite.distributed.WorkerJob;
import edu.upenn.cis.stormlite.spout.FileSpout;
import edu.upenn.cis.stormlite.spout.MyFileSpout;
import edu.upenn.cis.stormlite.tuple.Fields;
import edu.upenn.cis455.mapreduce.master.MasterServer;

public class SubmitJobRoute implements Route {
    static Logger log = LogManager.getLogger(SubmitJobRoute.class);

    private Config config;

    /**
     * create a WorkerJob using given configuration.
     * 
     * @param config, configuration about the job, should at least contain key:
     *                mapExecutors and key: reduceExecutors
     * @param job,    WorkerJob that needs to be populated.
     * @return true if succeed, false if not.
     */
    public static boolean createJob(Config config, WorkerJob job) {
        // create topology
        FileSpout fileSpout = new MyFileSpout();
        MapBolt mapBolt = new MapBolt();
        ReduceBolt reduceBolt = new ReduceBolt();
        MyPrintBolt printBolt = new MyPrintBolt();

        TopologyBuilder builder = new TopologyBuilder();
        builder.setSpout(MasterServer.FILE_SPOUT, fileSpout, 1);
        if (!config.containsKey("mapExecutors") || !config.containsKey("reduceExecutors"))
            return false;
        builder.setBolt(MasterServer.MAP_BOLT, mapBolt, Integer.valueOf(config.get("mapExecutors")))
                .fieldsGrouping(MasterServer.FILE_SPOUT, new Fields("value"));
        builder.setBolt(MasterServer.REDUCE_BOLT, reduceBolt, Integer.valueOf(config.get("reduceExecutors")))
                .fieldsGrouping(MasterServer.MAP_BOLT, new Fields("key"));
        builder.setBolt(MasterServer.PRINT_BOLT, printBolt, 1).firstGrouping(MasterServer.REDUCE_BOLT);

        Topology topo = builder.createTopology();

        // convert topology to a work job
        job.setConfig(config);
        job.setTopology(topo);
        return true;
    }

    /**
     * Send job to workers over HTTP request.
     * 
     * @param job,     WorkerJob
     * @param workers, a String[] of worker ip and port, e.g.,
     *                 ["http://localhost:45555", "http://localhost:8001"]
     * @return true if success, false if not.
     */
    public static boolean sendJob(WorkerJob job, String[] workers) {
        boolean success = true;
        try {
            int workerIndex = 0;
            for (String worker : workers) {
                log.info("Submitting job to worker adress:" + worker);
                job.getConfig().put("workerIndex", String.valueOf(workerIndex));
                // open connection with POST request to /definejob
                URL url = new URL(worker + "/definejob");
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setDoOutput(true);
                conn.setRequestMethod("POST");
                conn.setRequestProperty("Content-Type", "application/json");

                // write to stream the JSON-ized job object
                OutputStream os = conn.getOutputStream();
                ObjectMapper mapper = new ObjectMapper();
                mapper.enableDefaultTyping(ObjectMapper.DefaultTyping.NON_FINAL);
                os.write(mapper.writerWithDefaultPrettyPrinter().writeValueAsBytes(job));
                os.flush();

                // get response
                int statusCode = conn.getResponseCode();
                if (statusCode != HttpURLConnection.HTTP_OK) {
                    log.error("Failed to submit job to worker adress:" + worker + ", status code:" + statusCode);
                    success = false;
                }
                workerIndex += 1;
            }
        } catch (IOException e) {
            success = false;
            e.printStackTrace();
        }
        return success;
    }

    /**
     * Start workers.
     * 
     * @param workers, a String[] of worker ip and port, e.g.,
     *                 ["http://localhost:45555", "http://localhost:8001"]
     * @return true if success, false if not.
     */
    public static boolean startJob(String[] workers) {
        boolean success = true;
        try {
            for (String worker : workers) {
                // open connection with POST request to /definejob
                URL url = new URL(worker + "/runjob");
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setRequestMethod("POST");

                // get response
                int statusCode = conn.getResponseCode();
                if (statusCode != HttpURLConnection.HTTP_OK) {
                    success = false;
                }
            }
        } catch (IOException e) {
            success = false;
            e.printStackTrace();
        }
        return success;
    }

    public SubmitJobRoute(Config config) {
        this.config = config;
    }

    @Override
    public Object handle(Request request, Response response) throws Exception {
        // check if request contains all the information for constructing a config
        log.info("Received submitjob request from ip" + request.ip() + ", request string: " + request.queryString());
        if (request.queryParams("jobname") == null || request.queryParams("jobname").equals("")) {
            response.status(HttpURLConnection.HTTP_BAD_REQUEST);
            return "Bad request: missing jobname";
        }
        if (request.queryParams("classname") == null || request.queryParams("classname").equals("")) {
            response.status(HttpURLConnection.HTTP_BAD_REQUEST);
            return "Bad request: missing classname, example classname: edu.upenn.cis.cis455.mapreduce.job.MyJob";
        }
        if (request.queryParams("input") == null) {
            response.status(HttpURLConnection.HTTP_BAD_REQUEST);
            return "Bad request: missing input directory";
        }
        if (request.queryParams("output") == null) {
            response.status(HttpURLConnection.HTTP_BAD_REQUEST);
            return "Bad request: missing output directory";
        }
        if (request.queryParams("map") == null || request.queryParams("map").equals("")) {
            response.status(HttpURLConnection.HTTP_BAD_REQUEST);
            return "Bad request: missing map";
        }
        if (request.queryParams("reduce") == null || request.queryParams("reduce").equals("")) {
            response.status(HttpURLConnection.HTTP_BAD_REQUEST);
            return "Bad request: missing reduce";
        }

        log.info(String.format(
                "Received POST /submitjob request. jobname: %s, map: %s, reduce: %s, classname: %s, input: %s, output: %s",
                request.queryParams("jobname"),
                request.queryParams("map"),
                request.queryParams("reduce"),
                request.queryParams("classname"),
                request.queryParams("input"),
                request.queryParams("output")));

        // populate job config
        config.put("job", request.queryParams("jobname"));
        config.put("mapExecutors", request.queryParams("map"));
        config.put("reduceExecutors", request.queryParams("reduce"));
        config.put("mapClass", request.queryParams("classname"));
        config.put("reduceClass", request.queryParams("classname"));
        config.put("input", request.queryParams("input"));
        config.put("output", request.queryParams("output"));
        config.put("spoutExecutors", "1");
        config.put("workerList", MasterServer.getWorkerList());

        // create a worker job using config and send to all workers
        WorkerJob job = new WorkerJob();
        createJob(config, job);
        String[] workers = WorkerHelper.getWorkers(config);
        if (sendJob(job, workers)) {
            if (startJob(workers)) {
                return "Job submitted to all workers";
            } else {
                response.status(500);
                return "Job failed to run on all workers";
            }
        } else {
            response.status(500);
            return "Job failed to submit to all workers";
        }
    }
}
