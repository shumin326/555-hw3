package edu.upenn.cis455.mapreduce.worker.routes;

import spark.Request;
import spark.Response;
import spark.Route;

import java.io.IOException;
import java.util.List;

import com.fasterxml.jackson.databind.ObjectMapper;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import edu.upenn.cis.stormlite.DistributedCluster;
import edu.upenn.cis.stormlite.TopologyContext;
import edu.upenn.cis.stormlite.routers.StreamRouter;
import edu.upenn.cis.stormlite.tuple.Tuple;

public class PushDataRoute implements Route {

    static Logger log = LogManager.getLogger(PushDataRoute.class);
    private DistributedCluster cluster;
    private List<TopologyContext> contexts;

    public PushDataRoute(DistributedCluster cluster, List<TopologyContext> contexts) {
        this.cluster = cluster;
        this.contexts = contexts;
    }

    @Override
    public Object handle(Request request, Response response) throws Exception {
        try {
            String stream = request.params(":stream");
            final ObjectMapper om = new ObjectMapper();
            om.enableDefaultTyping(ObjectMapper.DefaultTyping.NON_FINAL);
            Tuple tuple = om.readValue(request.body(), Tuple.class);

            // Find the destination stream and route to it
            StreamRouter router = cluster.getStreamRouter(stream);

            if (contexts.isEmpty())
                log.error("No topology context -- were we initialized??");

            TopologyContext ourContext = contexts.get(contexts.size() - 1);

            // Instrumentation for tracking progress
            if (!tuple.isEndOfStream())
                ourContext.incSendOutputs(router.getKey(tuple.getValues()));

            // Handle tuple vs end of stream for our *local nodes only*
            // Please look at StreamRouter and its methods (execute, executeEndOfStream,
            // executeLocally, executeEndOfStreamLocally)
            if (!tuple.isEndOfStream()) {
                log.info("Worker received tuple: " + tuple + " from: " + tuple.getSourceExecutor() + " to " + stream);
                router.executeLocally(tuple, ourContext, tuple.getSourceExecutor());
            } else {
                log.info("Worker received EOS: " + tuple + " from: " + tuple.getSourceExecutor() + " to " + stream);
                router.executeEndOfStreamLocally(ourContext, tuple.getSourceExecutor());
            }

            return "OK";
        } catch (IOException e) {
            e.printStackTrace();
            response.status(500);
            return e.getMessage();
        }
    }

}
