package edu.upenn.cis455.mapreduce.worker;

import static spark.Spark.*;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import edu.upenn.cis.stormlite.DistributedCluster;
import edu.upenn.cis.stormlite.TopologyContext;
import edu.upenn.cis.stormlite.TopologyContext.STATE;
import edu.upenn.cis.stormlite.distributed.WorkerHelper;
import edu.upenn.cis.stormlite.distributed.WorkerJob;
import edu.upenn.cis455.mapreduce.worker.routes.DefineJobRoute;
import edu.upenn.cis455.mapreduce.worker.routes.PushDataRoute;
import edu.upenn.cis455.mapreduce.worker.routes.RunJobRoute;
import spark.Spark;

/**
 * Simple listener for worker creation
 * 
 * @author zives
 *
 */
public class WorkerServer {
    static Logger log = LogManager.getLogger(WorkerServer.class);

    private static DistributedCluster cluster = new DistributedCluster();
    private static List<TopologyContext> contexts = new ArrayList<>();
    private static List<String> topologies = new ArrayList<>();
    private static WorkerJob workerJob = new WorkerJob();

    public static String rootDirectory = ".";
    public static String inputDirectory = "";
    public static String outputDirectory = "";
    public static String job = "None";

    public WorkerServer(int myPort, String masterAddr, String rootDirectory) throws MalformedURLException {
        log.info("Creating server listener at socket " + myPort);

        port(myPort);

        if (!masterAddr.startsWith("http"))
            masterAddr = "http://" + masterAddr;
        String masterAddress = masterAddr;

        WorkerServer.rootDirectory = rootDirectory;
        Spark.post("/definejob", new DefineJobRoute(cluster, contexts, topologies, workerJob));

        Spark.post("/runjob", new RunJobRoute(cluster));

        Spark.post("/pushdata/:stream", new PushDataRoute(cluster, contexts));

        Spark.get("/shutdown", (req, res) -> {
            shutdown();
            Thread.sleep(2000);
            return "shutdown OK";
        });

        // Periodically sync with Master
        Timer timer = new Timer();
        timer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                String urlString = masterAddress + "/workerstatus";
                try {
                    urlString += "?port=" + myPort + "&status=" + getState() + "&job="
                            + URLEncoder.encode(job, "UTF-8");
                    urlString += "&keysRead=" + getKeysRead() + "&keysWritten=" + getKeysWritten() + "&results="
                            + URLEncoder.encode(getResults(), "UTF-8");
                    URL url = new URL(urlString);

                    HttpURLConnection con = (HttpURLConnection) url.openConnection();
                    con.setRequestMethod("GET");
                    int responseCode = con.getResponseCode();
                    if (responseCode != HttpURLConnection.HTTP_OK) {
                        System.out
                                .println("Failed to send GET /workerstatus to master. Response code : " + responseCode);
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }, 0, 10000);
    }

    public static void createWorker(Map<String, String> config, String masterAddr, String rootDirectory) {
        if (!config.containsKey("workerList"))
            throw new RuntimeException("Worker spout doesn't have list of worker IP addresses/ports");

        if (!config.containsKey("workerIndex"))
            throw new RuntimeException("Worker spout doesn't know its worker ID");
        else {
            String[] addresses = WorkerHelper.getWorkers(config);
            String myAddress = addresses[Integer.valueOf(config.get("workerIndex"))];

            log.debug("Initializing worker " + myAddress);

            URL url;
            try {
                url = new URL(myAddress);

                new WorkerServer(url.getPort(), masterAddr, rootDirectory);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
        }
    }

    public static void shutdown() {
        synchronized (topologies) {
            for (String topo : topologies)
                cluster.killTopology(topo);
        }

        cluster.shutdown();
    }

    /**
     * get worker server state
     * 
     * @return String in {"INIT", "MAPPING", "REDUCING", "IDLE"}
     */
    public static String getState() {
        if (contexts.size() == 0)
            return "IDLE";
        TopologyContext context = contexts.get(contexts.size() - 1);
        switch (context.getState()) {
            case IDLE:
                return "IDLE";
            case INIT:
                return "INIT";
            case MAPPING:
                return "MAPPING";
            case REDUCING:
                return "REDUCING";
        }
        return "IDLE";
    }

    /**
     * get worker server keysRead
     * 
     * @return keys read
     */
    public static int getKeysRead() {
        if (getState().equals("IDLE"))
            return 0;
        return contexts.get(contexts.size() - 1).getKeysRead();
    }

    /**
     * get worker server keysWritten
     * 
     * @return keys written
     */
    public static int getKeysWritten() {
        if (contexts.size() == 0)
            return 0;
        return contexts.get(contexts.size() - 1).getKeysWritten();
    }

    public static String getResults() {
        if (contexts.size() == 0)
            return "[]";
        TopologyContext context = contexts.get(contexts.size() - 1);
        if (context.getState() == STATE.IDLE) {
            return context.getResults();
        }
        return "[]";
    }

    /**
     * Simple launch for worker server. Note that you may want to change / replace
     * most of this.
     * 
     * @param args
     * @throws MalformedURLException
     */
    public static void main(String args[]) throws MalformedURLException {
        if (args.length < 3) {
            System.out.println("Usage: WorkerServer [port number] [master host/IP]:[master port] [storage directory]");
            System.exit(1);
        }

        int myPort = Integer.valueOf(args[0]);
        String masterAddr = args[1];
        String rootDirectory = args[2];

        System.out.println("Worker node startup, on port " + myPort);

        new WorkerServer(myPort, masterAddr, rootDirectory);

    }
}
