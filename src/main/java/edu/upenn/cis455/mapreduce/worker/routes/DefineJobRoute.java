package edu.upenn.cis455.mapreduce.worker.routes;

import spark.Request;
import spark.Response;
import spark.Route;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

import com.fasterxml.jackson.databind.ObjectMapper;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import edu.upenn.cis.stormlite.DistributedCluster;
import edu.upenn.cis.stormlite.TopologyContext;
import edu.upenn.cis.stormlite.distributed.WorkerJob;
import edu.upenn.cis455.mapreduce.worker.WorkerServer;

public class DefineJobRoute implements Route {
    static Logger log = LogManager.getLogger(DefineJobRoute.class);
    private DistributedCluster cluster;
    private List<TopologyContext> contexts;
    private List<String> topologies;
    private WorkerJob workerJob;

    public DefineJobRoute(DistributedCluster cluster, List<TopologyContext> contexts, List<String> topologies,
            WorkerJob workerJob) {
        this.cluster = cluster;
        this.contexts = contexts;
        this.topologies = topologies;
        this.workerJob = workerJob;
    }

    /**
     * Helper function for creating a directory, recursively.
     * 
     * @param directory, e.g. /home/usr/foo/bar
     */
    public static void createDirectory(String directory) {
        String[] arr = directory.split("/");
        String path = "";
        for (String dir : arr) {
            if (dir.length() > 0) {
                path += dir + "/";
                if (!Files.exists(Paths.get(path))) {
                    try {
                        Files.createDirectory(Paths.get(path));
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }

    @Override
    public Object handle(Request request, Response response) throws Exception {
        final ObjectMapper om = new ObjectMapper();
        om.enableDefaultTyping(ObjectMapper.DefaultTyping.NON_FINAL);
        try {
            workerJob = om.readValue(request.body(), WorkerJob.class);
            // Get input directory and output directory, and make sure it exists on disk.
            if (!workerJob.getConfig().containsKey("input")) {
                log.warn("Worker server config does not have an input directory!");
            } else if (workerJob.getConfig().get("input").equals("")) {
                WorkerServer.inputDirectory = WorkerServer.rootDirectory;
            } else {
                WorkerServer.inputDirectory = WorkerServer.rootDirectory + "/" + workerJob.getConfig().get("input");
            }
            if (!workerJob.getConfig().containsKey("output")) {
                log.warn("Worker server config does not have an output directory!");
            } else if (workerJob.getConfig().get("output").equals("")) {
                WorkerServer.outputDirectory = WorkerServer.rootDirectory;
            } else {
                WorkerServer.outputDirectory = WorkerServer.rootDirectory + "/" + workerJob.getConfig().get("output");
            }
            if (!workerJob.getConfig().containsKey("job")) {
                log.warn("Worker server config does not have an job name!");
            } else {
                WorkerServer.job = workerJob.getConfig().get("job");
            }

            createDirectory(WorkerServer.inputDirectory);
            createDirectory(WorkerServer.outputDirectory);

            try {
                log.info("Processing job definition request " + workerJob.getConfig().get("job") +
                        " on machine " + workerJob.getConfig().get("workerIndex"));
                contexts.add(cluster.submitTopology(workerJob.getConfig().get("job"), workerJob.getConfig(),
                        workerJob.getTopology()));

                // Add a new topology
                synchronized (topologies) {
                    topologies.add(workerJob.getConfig().get("job"));
                }
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }

            return "Job launched";
        } catch (IOException e) {
            e.printStackTrace();

            // Internal server error
            response.status(500);
            return e.getMessage();
        }
    }

}
