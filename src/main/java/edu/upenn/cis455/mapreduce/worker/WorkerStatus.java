package edu.upenn.cis455.mapreduce.worker;

import java.time.Instant;

import edu.upenn.cis.stormlite.TopologyContext.STATE;

public class WorkerStatus {
    private STATE state = STATE.INIT;
    private String job = "";
    private int keysRead = 0;
    private int keysWritten = 0;
    private String results = "";
    private long lastUpdatedTime = -1;
    private int port = -1;
    private int index = -1;

    public WorkerStatus() {
    }

    public WorkerStatus(STATE state, String job, int keysRead, int keysWritten, String results, int port, int index) {
        this.index = index;
        update(state, job, keysRead, keysWritten, results, port, index);
    }

    public void update(STATE state, String job, int keysRead, int keysWritten, String results, int port, int index) {
        this.state = state;
        this.job = job;
        this.keysRead = keysRead;
        this.keysWritten = keysWritten;
        this.results = results;
        this.port = port;
        lastUpdatedTime = Instant.now().toEpochMilli();
        this.index = index;
    }

    /**
     * Check if a worker is active, i.e., reported less than 30 seconds ago.
     * 
     * @return
     */
    public boolean isActive() {
        return Instant.now().toEpochMilli() - lastUpdatedTime < 30000;
    }

    public STATE getState() {
        return state;
    }

    public String getJob() {
        return job;
    }

    public int getKeysRead() {
        return keysRead;
    }

    public int getKeysWritten() {
        return keysWritten;
    }

    public String getResults() {
        return results;
    }

    public int getPort() {
        return port;
    }

    public int getIndex() {
        return index;
    }
}
