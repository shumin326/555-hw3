package edu.upenn.cis455.storage;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import com.sleepycat.persist.model.Entity;
import com.sleepycat.persist.model.PrimaryKey;

/**
 * Entity class for reduce job cache, i.e., (key, values) pairs
 */
@Entity
public class Cache {
    @PrimaryKey
    private String key;

    private List<String> values;

    public Cache(String key) {
        this.key = key;
        values = new LinkedList<>();
    }

    public void insertValue(String value) {
        values.add(value);
    }

    public Iterator<String> getValues() {
        return values.iterator();
    }

    private Cache() {
    }
}
