package edu.upenn.cis455.storage;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Locale;
import java.util.Set;
import java.util.TimeZone;
import com.sleepycat.je.Environment;
import com.sleepycat.je.EnvironmentConfig;
import com.sleepycat.persist.EntityCursor;
import com.sleepycat.persist.EntityStore;
import com.sleepycat.persist.PrimaryIndex;
import com.sleepycat.persist.StoreConfig;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Storage {
    Logger logger = LogManager.getLogger(Storage.class);

    private static Environment env;
    private static EntityStore cacheStore;
    private static PrimaryIndex<String, Cache> cacheByKey;

    public Storage(String directory) {
        // Set environment configuration.
        EnvironmentConfig envConfig = new EnvironmentConfig();
        envConfig.setAllowCreate(true);
        envConfig.setTransactional(true);
        envConfig.setConfigParam(EnvironmentConfig.CHECKPOINTER_HIGH_PRIORITY, "true");
        env = new Environment(new File(directory), envConfig);

        // Set store configuration.
        StoreConfig storeConfig = new StoreConfig();
        storeConfig.setAllowCreate(true);
        storeConfig.setTransactional(true);

        // Create cache storage entity and its index.
        cacheStore = new EntityStore(env, "cacheStore", storeConfig);
        cacheByKey = cacheStore.getPrimaryIndex(String.class, Cache.class);
    }

    /**
     * Add a (key, value) pair into the cache storage.
     * 
     * @param key
     * @param value
     */
    public synchronized void addCache(String key, String value) {
        if (key == null || value == null)
            return;
        Cache cache;
        if (!cacheByKey.contains(key)) {
            cache = new Cache(key);
        } else {
            cache = cacheByKey.get(key);
        }
        cache.insertValue(value);
        cacheByKey.put(cache);
    }

    /**
     * Get all keys present in the storage.
     * 
     * @return set of keys.
     */
    public synchronized Set<String> getKeys() {
        Set<String> keys = new HashSet<>();
        EntityCursor<String> cursor = cacheByKey.keys();
        try {
            for (String key = cursor.first(); key != null; key = cursor.next()) {
                keys.add(key);
            }
        } finally {
            cursor.close();
        }
        return keys;
    }

    /**
     * Get all values for a certain key stored in cache.
     * 
     * @param key
     * @return an iterator for values. Null if key is not present.
     */
    public synchronized Iterator<String> getValues(String key) {
        if (cacheByKey.contains(key)) {
            return cacheByKey.get(key).getValues();
        }
        return null;
    }

    /**
     * Shuts down / flushes / closes the storage system
     */
    public void close() {
        clear();
        cacheStore.close();
        env.close();
    }

    /**
     * Clear all storage.
     */
    public void clear() {
        cacheStore.truncateClass(Cache.class);
    }
}
