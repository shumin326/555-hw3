package edu.upenn.cis455.storage;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

public class StorageFactory {
    public static Storage getDatabaseInstance(String directory) {
        if (!Files.exists(Paths.get(directory))) {
            try {
                Files.createDirectory(Paths.get(directory));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return new Storage(directory);
    }
}
