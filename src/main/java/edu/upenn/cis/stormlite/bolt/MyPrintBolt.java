package edu.upenn.cis.stormlite.bolt;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import edu.upenn.cis.stormlite.OutputFieldsDeclarer;
import edu.upenn.cis.stormlite.TopologyContext;
import edu.upenn.cis.stormlite.TopologyContext.STATE;
import edu.upenn.cis.stormlite.distributed.ConsensusTracker;
import edu.upenn.cis.stormlite.distributed.WorkerHelper;
import edu.upenn.cis.stormlite.routers.StreamRouter;
import edu.upenn.cis.stormlite.tuple.Fields;
import edu.upenn.cis.stormlite.tuple.Tuple;
import edu.upenn.cis455.mapreduce.worker.WorkerServer;

/**
 * A trivial bolt that simply outputs its input stream to the
 * console
 * 
 * @author zives
 *
 */
public class MyPrintBolt implements IRichBolt {
	static Logger log = LogManager.getLogger(MyPrintBolt.class);

	Fields myFields = new Fields();

	/**
	 * To make it easier to debug: we have a unique ID for each
	 * instance of the PrintBolt, aka each "executor"
	 */
	private String executorId = UUID.randomUUID().toString();

	private TopologyContext context;
	private ConsensusTracker votesForEos;
	private BufferedWriter writer;

	@Override
	public void cleanup() {
		// Do nothing

	}

	@Override
	public boolean execute(Tuple input) {
		if (context.getState() == STATE.IDLE) {
			if (!input.isEndOfStream()) {
				throw new RuntimeException("PrintBolt:" + executorId + " received data:" + input.toString()
						+ "from ReduceBolt:" + input.getSourceExecutor()
						+ " after it thought the stream had ended!");
			}
			// Already done
			return false;
		} else if (input.isEndOfStream()) {
			// all source reducers have finished
			log.info("PrintBolt:" + executorId + " is processing EOS from " + input.getSourceExecutor());
			if (votesForEos.voteForEos(input.getSourceExecutor())) {
				log.info("PrintBolt:" + executorId + " has received enough EOS and finished");
				context.setState(STATE.IDLE);
				try {
					writer.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
				return false;
			}
			// keep waiting for other source reducers to finish.
			return true;
		} else {
			log.info("PrintBolt:" + executorId + " is printing value: "
					+ String.format("(%s,%s)", input.getObjectByField("key"), input.getObjectByField("value")));
			// Write to output.txt
			synchronized (writer) {
				List<Object> output = input.getValues();
				try {
					writer.write("(" + (String) output.get(0) + ", " + (String) output.get(1) + ")\n");
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			// update context
			context.addResult(String.format("(%s,%s)", input.getObjectByField("key"), input.getObjectByField("value")));
		}
		System.out.println(getExecutorId() + ": " + input.toString());
		return true;
	}

	@Override
	public void prepare(Map<String, String> stormConf, TopologyContext context, OutputCollector collector) {
		this.context = context;

		String filename = WorkerServer.outputDirectory + "/output.txt";

		try {
			writer = new BufferedWriter(new FileWriter(filename));
		} catch (IOException e) {
			e.printStackTrace();
		}
		int reduceExecutors = Integer.valueOf(stormConf.get("reduceExecutors"));
		int workers = WorkerHelper.getWorkers(stormConf).length;
		votesForEos = new ConsensusTracker(workers * reduceExecutors);
	}

	@Override
	public String getExecutorId() {
		return executorId;
	}

	@Override
	public void setRouter(StreamRouter router) {
		// Do nothing
	}

	@Override
	public void declareOutputFields(OutputFieldsDeclarer declarer) {
		declarer.declare(myFields);
	}

	@Override
	public Fields getSchema() {
		return myFields;
	}

}
