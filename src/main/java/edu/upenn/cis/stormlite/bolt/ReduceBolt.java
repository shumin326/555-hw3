package edu.upenn.cis.stormlite.bolt;

import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import edu.upenn.cis.stormlite.OutputFieldsDeclarer;
import edu.upenn.cis.stormlite.TopologyContext;
import edu.upenn.cis.stormlite.TopologyContext.STATE;
import edu.upenn.cis.stormlite.distributed.ConsensusTracker;
import edu.upenn.cis.stormlite.distributed.WorkerHelper;
import edu.upenn.cis.stormlite.routers.StreamRouter;
import edu.upenn.cis.stormlite.tuple.Fields;
import edu.upenn.cis.stormlite.tuple.Tuple;
import edu.upenn.cis455.mapreduce.Job;
import edu.upenn.cis455.mapreduce.worker.WorkerServer;
import edu.upenn.cis455.storage.Storage;
import edu.upenn.cis455.storage.StorageFactory;

/**
 * A simple adapter that takes a MapReduce "Job" and calls the "reduce"
 * on a per-tuple basis
 * 
 */
/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

public class ReduceBolt implements IRichBolt {
	static Logger log = LogManager.getLogger(ReduceBolt.class);

	Job reduceJob;

	/**
	 * This object can help determine when we have
	 * reached enough votes for EOS
	 */
	ConsensusTracker votesForEos;

	/**
	 * To make it easier to debug: we have a unique ID for each
	 * instance of the WordCounter, aka each "executor"
	 */
	String executorId = UUID.randomUUID().toString();

	Fields schema = new Fields("key", "value");

	boolean sentEos = false;

	/**
	 * This is where we store the buffered (key, values) results before doing
	 * reducing job. Unique for each reduce bolt.
	 */
	String rootDirectory;
	Storage db;

	/**
	 * This is where we send our output stream
	 */
	private OutputCollector collector;

	private TopologyContext context;

	public ReduceBolt() {
		if (WorkerServer.rootDirectory.equals("")) {
			rootDirectory = "./" + executorId; // Each bolt is a new thread, create an individual storage directory.
			log.warn("Worker server is not initialized to have a root directory!");
		}
		rootDirectory = WorkerServer.rootDirectory + "/" + executorId;
		db = StorageFactory.getDatabaseInstance(rootDirectory);
	}

	/**
	 * Initialization, just saves the output stream destination
	 */
	@Override
	public void prepare(Map<String, String> stormConf,
			TopologyContext context, OutputCollector collector) {
		this.collector = collector;
		this.context = context;

		if (!stormConf.containsKey("reduceClass"))
			throw new RuntimeException("Mapper class is not specified as a config option");
		else {
			String mapperClass = stormConf.get("reduceClass");

			try {
				reduceJob = (Job) Class.forName(mapperClass).newInstance();
			} catch (InstantiationException | IllegalAccessException | ClassNotFoundException e) {
				e.printStackTrace();
				throw new RuntimeException("Unable to instantiate the class " + mapperClass);
			}
		}
		if (!stormConf.containsKey("mapExecutors")) {
			throw new RuntimeException("Reducer class doesn't know how many map bolt executors");
		}

		// Determine how many EOS votes needed and set up ConsensusTracker (or however
		// you want to handle consensus)
		int workers = WorkerHelper.getWorkers(stormConf).length;
		int mapExecutors = Integer.valueOf(stormConf.get("mapExecutors"));
		votesForEos = new ConsensusTracker(workers * mapExecutors);
	}

	/**
	 * Process a tuple received from the stream, buffering by key
	 * until we hit end of stream
	 */
	@Override
	public synchronized boolean execute(Tuple input) {
		if (sentEos) {
			if (!input.isEndOfStream())
				throw new RuntimeException("ReduceBolt:" + executorId + " received data:" + input.toString()
						+ "from MapBolt:" + input.getSourceExecutor()
						+ " after it thought the stream had ended!");
			// Already done!
			return false;
		} else if (input.isEndOfStream()) {
			log.info("ReduceBolt:" + executorId + "is processing EOS from " + input.getSourceExecutor());
			// Only if at EOS do we trigger the reduce operation over what's in
			// BerkeleyDB for the associated key, and output all state
			if (votesForEos.voteForEos(input.getSourceExecutor())) {
				log.info(String.format("ReduceBolt:" + executorId + "has recived enough EOS and starts reducing"));
				if (context.getState() != STATE.REDUCING) {
					context.setState(STATE.REDUCING);
					context.clear();
				}
				// Read all cached (key, values) from database
				Set<String> keys = db.getKeys();
				for (String key : keys) {
					Iterator<String> values = db.getValues(key);
					context.incKeysRead();
					reduceJob.reduce(key, values, collector, executorId);
					context.incKeysWritten();
					context.incReduceOutputs(key);
				}
				// Send EOS because its job has all done.
				collector.emitEndOfStream(executorId);
				log.info(String.format("ReduceBolt:" + executorId + "has done reducing and sent EOS"));
				sentEos = true;
			}
		} else {
			// Collect the tuples by key into BerkeleyDB (until EOS arrives, in the above
			// condition)
			log.info("ReduceBolt:" + executorId + "is collecting " + input.toString() + " from "
					+ input.getSourceExecutor());
			String key = input.getStringByField("key");
			String value = input.getStringByField("value");
			if (!key.equals(""))
				db.addCache(key, value);
		}
		return true;
	}

	/**
	 * Shutdown, just frees memory
	 */
	@Override
	public void cleanup() {
		db.close();
	}

	/**
	 * Lets the downstream operators know our schema
	 */
	@Override
	public void declareOutputFields(OutputFieldsDeclarer declarer) {
		declarer.declare(schema);
	}

	/**
	 * Used for debug purposes, shows our exeuctor/operator's unique ID
	 */
	@Override
	public String getExecutorId() {
		return executorId;
	}

	/**
	 * Called during topology setup, sets the router to the next
	 * bolt
	 */
	@Override
	public void setRouter(StreamRouter router) {
		this.collector.setRouter(router);
	}

	/**
	 * The fields (schema) of our output stream
	 */
	@Override
	public Fields getSchema() {
		return schema;
	}

}
