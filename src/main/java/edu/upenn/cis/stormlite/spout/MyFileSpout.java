package edu.upenn.cis.stormlite.spout;

import edu.upenn.cis455.mapreduce.worker.WorkerServer;

public class MyFileSpout extends FileSpout {

    @Override
    public String getFilename() {
        return WorkerServer.inputDirectory;
    }

}
